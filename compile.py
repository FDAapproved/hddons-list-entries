#!/usr/bin/env python

import os
import re
import sys
import json
import markdown
from markdown.preprocessors import Preprocessor
from markdown.extensions import Extension
from ruamel.yaml import YAML
from datetime import datetime

class DropdownPreprocessor(Preprocessor):
	D1_RE = re.compile(r".*\n<<<", re.MULTILINE)
	D2_RE = re.compile(r"<<<", re.MULTILINE)

	def run(self, lines):
		text = "\n".join(lines)
		start = True
		while 1:
			if start:
				m = self.D1_RE.search(text)
			else:
				m = self.D2_RE.search(text)

			if m:
				if start:
					content = m.group().split("\n")
					tmp = self.md.htmlStash.store(
						"<details>\n<summary>{}</summary>".format(content[0])
					)
					start = False
				else:
					tmp = self.md.htmlStash.store("</details>")
					start = True

				text = "{}\n{}\n{}".format(
					text[:m.start()],
					tmp,
					text[m.end():]
				)

			else:
				break

		return text.split("\n")

class IconLinkPreprocessor(Preprocessor):
	ICONLINK_RE = re.compile(r".+\|.+\|.+", re.MULTILINE)

	def run(self, lines):
		text = "\n".join(lines)
		while 1:
			m = self.ICONLINK_RE.search(text)

			if m:
				content = m.group().split("|")
				tmp = self.md.htmlStash.store(
					"<div class=iconlink><a target=_blank href={href}><img src={img}>{text}</a></div>".format(
						href = content[2],
						img = content[1],
						text = content[0]
					)
				)
				text = "{}\n{}\n{}".format(
					text[:m.start()],
					tmp,
					text[m.end():]
				)

			else:
				break

		return text.split("\n")

class Dastension(Extension):
	def extendMarkdown(self, md):
		md.preprocessors.register(IconLinkPreprocessor(md), "iconlink", 25)
		md.preprocessors.register(DropdownPreprocessor(md), "dropdown", 25)

def processMarkdown(text):
	return markdown.markdown(text, output_format="html5", extensions=['nl2br', 'fenced_code', 'pymdownx.tilde', Dastension()])

# Main
directory = ""

# Nothing entered / Not enough arguments
if len(sys.argv) < 2:
	print("Usage:")
	print("compile.py <category>")
	print("")
	print("Outputs to ./list_output/")
	exit()

# Too many arguments
if len(sys.argv) > 2:
	print("You provided more than 1 directory. Don't do that. Ignoring the other directories.")

subdirectory = sys.argv[1]
files = []
output_path = ""

try:
	os.mkdir("list_output")
except OSError as error:
	print(error)

output_path = os.path.join("list_output", subdirectory + "_list.json")

# Get files
files = [os.path.join(subdirectory, file) for file in os.listdir(subdirectory) if os.path.isfile(os.path.join(subdirectory, file)) and file != "about.txt"]
files.sort()
print(str(files))

# Get files contents

date = datetime.now().strftime("%m/%d/%Y")
result = '{"last_updated":"' + date + '",'

# Get the About file
with open(os.path.join(subdirectory, "about.txt"), "r") as file:
	print("Reading about.txt")
	result += json.dumps({"about":processMarkdown(file.read())})[1:-1] + ","

entries = []
entry_headers = []

result += '"list":{'
for f in files:
	print("Reading " + f)

	with open(f, "r") as file:
		contents = YAML(typ="safe").load(file.read())

		# Translate Markdown
		contents["description"] = processMarkdown(contents["description"])

		if "flairs" in list(contents):
			flairs = contents["flairs"]
			contents["flairs"] = {}
			for i in flairs:
				contents["flairs"][processMarkdown(i)] = flairs[i]
			print(contents["flairs"])

		entries.append(contents)
		entry_headers.append("{}|{}".format(
			contents["title"],
			contents["credits"]
		))

entry_headers.sort()

first = True
count = 0

# Store as json
for head in entry_headers:
	tnc = head.split("|")
	for entry in entries:
		if entry["title"] == tnc[0] and entry["credits"] == tnc[1]:
			if not first:
				result += ','
			result += '"{}":'.format(str(count))
			result += json.dumps(entry)
			first = False
			count += 1
			break

result += '}}'

# Wait a minute... Isn't this just JSON format?
# Yes. Now export it for the website to read :]
with open(output_path, "w") as l:
	l.write(result)
	print("Wrote to " + output_path)
